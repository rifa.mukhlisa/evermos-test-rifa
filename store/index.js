import Vuex from 'vuex';
import { state, mutations, actions } from './products';

const createStore = () => {
  return new Vuex.Store({
    state,
    mutations,
    actions,
  });
};

export default createStore;
