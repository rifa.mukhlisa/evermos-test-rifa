import axios from 'axios'

export const state = () => ({
  products: [],
  product: {},
  isError: false,
  isLoading: true,
})

export const mutations = {
  updateProducts(state, payload) {
    state.products = payload
  },
  updateProduct(state, payload) {
    state.product = payload
  },
  changeIsLoading(state, payload) {
    state.isLoading = payload
  },
  changeIsError(state, payload) {
    state.isError = payload
  },
}

export const actions = {
  async getProductLists({ commit }) {
    commit('changeIsLoading', true)
    commit('changeIsError', false)
    try {
      const { data } = await axios.get(
        `https://fakestoreapi.com/products`
      )
      commit('updateProducts', data || [])
    } catch (e) {
      commit('changeIsError', true)
    } finally {
      commit('changeIsLoading', false)
    }
  },

  async getProductById({ commit }, id) {
    commit('changeIsLoading', true)
    commit('changeIsError', false)
    try {
      const { data } = await axios.get(
        `https://fakestoreapi.com/products/${id}`
      )
      commit('updateProduct', data || [])
    } catch (e) {
      commit('changeIsError', true)
    } finally {
      commit('changeIsLoading', false)
    }
  },
}
