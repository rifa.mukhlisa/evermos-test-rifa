const menuItems = [
  {
    title: 'New & Featured',
    submenu: [
      { title: 'Running Shoes', route: '/products/new/running' },
      { title: 'Sneakers', route: '/products/new/sneakers' },
      { title: 'Casual Shoes', route: '/products/new/casual' },
      { title: 'Sports Shoes', route: '/products/new/sports' },
      { title: 'Outdoor Shoes', route: '/products/new/outdoor' },
      { title: 'Premium Collection', route: '/products/featured/premium' },
      { title: 'Limited Edition', route: '/products/featured/limited' },
      { title: 'Best Sellers', route: '/products/featured/best-sellers' },
      { title: 'Designer Picks', route: '/products/featured/designer' },
      { title: 'Athletic Performance', route: '/products/featured/athletic' }
    ],
  },
  {
    title: 'Men',
    submenu: [
      { title: 'Shoes', route: '/products/men/shoes' },
      { title: 'Clothing', route: '/products/men/clothing' },
      { title: 'Accesories & Equipment', route: '/products/men/accesories' },
      { title: 'Shop by Brand', route: '/products/men/brand' },
    ]
  },
  {
    title: 'Women',
    submenu: [
      { title: 'Shoes', route: '/products/women/shoes' },
      { title: 'Clothing', route: '/products/women/clothing' },
      { title: 'Accesories & Equipment', route: '/products/women/accesories' },
      { title: 'Shop by Brand', route: '/products/women/brand' },
    ]
  },
  {
    title: 'Kids',
    submenu: [
      { title: 'Boys Shoes', route: '/products/kids/boyshoes' },
      { title: 'Girls Shoes', route: '/products/kids/girlsshoes' },
      { title: 'Boys Clothing', route: '/products/kids/boyclothing' },
      { title: 'Girls Clothing', route: '/products/kids/girlclothing' },
    ]
  },
  {
    title: 'Sale',
  },
  {
    title: 'SNKRS',
  }
]

export default menuItems;
