import Vue from 'vue';
import formatCurrency from '~/helpers/currency';

Vue.filter('currency', function (value, currencyCode = 'USD') {
  return formatCurrency(value, currencyCode);
});
