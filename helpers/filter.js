import Vue from 'vue';
import formatCurrency from './currency';

Vue.filter('currency', function (value, currencyCode = 'USD') {
  return formatCurrency(value, currencyCode);
});
